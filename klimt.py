# Copyright UCA/CNRS/Inria
# Contributor(s): Cedric Dubois 2022
#
# cedric.dubois@inria.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#
#
import numpy as np
from scipy.spatial import distance_matrix
import warnings
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils.class_weight import compute_sample_weight
from sklearn.neighbors import KNeighborsClassifier as KNNC
from scipy.sparse import coo_array, csr_matrix, csr_array

# EPSILON = 1e-8
# NPY_INFINITY = np.inf

class KLIMT(BaseEstimator, TransformerMixin):
    """
    KLIMT: Kernel, on Local and Interpretable Metric, for Transformation
    Easily Interpretable, Non-parametric Sample Transformation for Classification
    WARNING: Only the Gaussian kernel is implemented yet
    """
    def __init__(self,
                 gamma=1,
                 k_nn=100,
                 class_weight=None,
                 raise_warning=True,
                 verbose=0,
                 ):
        """
        Parameters
        ----------
        :param gamma: float, inverse kernel width
        :param k_nn: int, number of nearest neighbors to use for estimating the translations
        :param class_weight: str, None or 'balanced'
        :param raise_warning: bool
        :param verbose: int
        """
        #
        self.gamma = gamma
        self.k_nn = k_nn
        self.raise_warning = raise_warning
        self.class_weight = class_weight
        self.verbose = verbose

        self.k_nn_model = None
        self.saved_X = False


    def fit(self, X, y, targets='barycenter'):
        """
        Fit X transformation based on y.
        :param X: array, shape (n_samples, n_features). Samples to fit
        :param y: array, shape (n_samples). Target labels
        :param targets: array (n_samples) or str 'barycenter', specific targets
        :return:
        """
        self.classes_ = np.unique(y)
        self.n_classes_ = self.classes_.shape[0]
        if isinstance(targets, str):
            if targets=='barycenter':
                if self.verbose>0:
                    print("[KLIMT] Using class barycenter as targets")
                targets = np.empty([self.n_classes_, X.shape[1]])
                k=0
                for lab in self.classes_:
                    targets[k] = np.mean(X[y==lab], axis=0)
                    k+=1
            elif targets=='equidistant' or targets=='eq':
                raise NotImplementedError("Not implemented yet, please provide targets manually")
        elif isinstance(targets, csr_matrix | csr_array | np.ndarray):
            if self.verbose > 0:
                print("[KLIMT] Using provided targets")
        else:
            raise NotImplementedError("Provide targets as scipy csr_matrix or scipy csr_array or numpy ndarray or string")
        #
        self.sample_classes_train = y
        #
        self.samples_train = X
        #
        self.targets = targets
        self.ordered_targets = targets
        #
        self.translation = targets[y] - self.samples_train
        #
        k_nn = self.k_nn
        k_max = X.shape[0]
        if k_nn == None or k_nn > k_max:
            k_nn = k_max

        # TODO: Change this classifer to a simple graph construction (but this method seems faster)
        self.k_nn_model = KNNC(n_neighbors=k_nn,
                               n_jobs=-1,
                               leaf_size=40,
                               algorithm='auto',
                               )
        self.k_nn_model.fit(self.samples_train, self.sample_classes_train)

        return self

    def fit_old(self, X, y, sample_target=None):
        """
        Fit X transformation to centroid based on y.
        :param X: array, shape (n_samples, n_features). Samples to fit
        :param y: array, shape (n_samples). Target labels
        :param sample_target: array (n_samples), specific targets
        :return:
        """
        self.classes_ = np.unique(y)
        self.n_classes_ = self.classes_.shape[0]
        #
        self.samples_train = X
        self.sample_classes_train = y
        n_samples_train_translation = self.samples_train.shape[0]
        n_features = self.samples_train.shape[1]
        self.samples_train = self.samples_train.copy()

        target_coordinates_extd = np.empty([self.n_classes_, n_features])
        self.ordered_targets = np.empty_like(target_coordinates_extd)
        dict_lab_to_idx = {}
        k=0
        for lab in self.classes_:
            target_coordinates_extd[k] = np.mean(self.samples_train[self.sample_classes_train==lab], axis=0)
            dict_lab_to_idx[lab] = k
            k+=1

        if isinstance(sample_target, csr_matrix or csr_array):
            if self.verbose:
                print("[KLIMT] Using pre-loaded sample_target")
            # target_coordinates_extd = np.unique(sample_target, axis=0)
            target_coordinates_extd = np.unique(sample_target.toarray(), axis=0)
        elif isinstance(sample_target, np.ndarray):
            if self.verbose:
                print("[KLIMT] Using pre-loaded sample_target")
            target_coordinates_extd = np.unique(sample_target, axis=0)
        else:
            if self.verbose:
                print("[KLIMT] Starting compute barycenters")
            sample_target = np.zeros([n_samples_train_translation, n_features])
            for lab in self.classes_:
                mask = (self.sample_classes_train == lab)
                sample_target[mask] = target_coordinates_extd[dict_lab_to_idx[lab]]

        #
        self.translation = sample_target - self.samples_train # TODO : Remove here
        self.sample_targets = sample_target
        self.targets = target_coordinates_extd
        self.ordered_targets = target_coordinates_extd
        self.dict_lab_to_idx = dict_lab_to_idx
        #
        # Fit beta_i
        k_nn = self.k_nn
        k_max = self.samples_train.shape[0]
        if k_nn == None or k_nn > k_max:
            k_nn = k_max

        # TODO: Change this classifier to a simple graph construction (but this method seems faster)
        self.k_nn_model = KNNC(n_neighbors=k_nn,
                               n_jobs=-1,
                               leaf_size=40,
                               algorithm='auto', )
        self.k_nn_model.fit(self.samples_train, self.sample_classes_train)

        return self

    def transform(self, X, k_nn=None, gamma=None, loocv=False):
        """
        transform samples according to training data.
        Parameters
        ----------
        :param X: samples to transform
        :param k_nn (optional): int, number of nearest neighbors to use for estimating the translations
        :param gamma (optional): float, kernel inverse scaling factor
        :return: transformed samples
        """
        if self.k_nn==None:
            return self.transform_exact(X, loocv=loocv, gamma=gamma)
        else:
            return self.transform_approx(X, k_nn=k_nn, gamma=gamma, loocv=loocv)

    def transform_exact(self, X, loocv=False, gamma=None):
        """ Weighted knn like
        :param X:
        :return:
        """
        if gamma==None:
            gamma = self.gamma
        if loocv:
            if np.array_equal(X, None): # ToDo: not optimal
                X = self.samples_train
            assert np.array_equal(X, self.samples_train)
        #
        if self.raise_warning:
            warnings.warn("k_nn param not provided, computing transformation from all neighbors. "
                          f"This compute the matrix of size {self.samples_train.shape[0]} x {X.shape[0]}.")

        assert X.shape[1] == self.samples_train.shape[1]

        distances = distance_matrix(X, self.samples_train)**2
        alpha_ = np.exp(- gamma * distances)
        del distances
        if self.class_weight == 'balanced':
            sample_weights = compute_sample_weight('balanced', y=self.sample_classes_train)
            alpha_ *= sample_weights.T
        #
        norm_ = alpha_.sum(axis=1, keepdims=True)
        norm_[norm_==0] = 1
        alpha_ /= norm_
        #
        if loocv:
            alpha_[:,0] = 0
        #
        self.alpha = alpha_

        new_trans = alpha_ @ self.translation

        return X + new_trans

    def search_neighbors(self, X, k_nn):
        """
        Neighbors search
        :param X: array, samples
        :return: distances, mask
        """
        if np.array_equal(X, self.saved_X):
            if self.verbose:
                print("[KLIMT] Using pre-computed neighbors distances")
            return self.distances, self.mask
        else:
            if self.verbose:
                print("[KLIMT] Searching for neighbors with sklearn ...")
            self.distances, self.mask = self.k_nn_model.kneighbors(X, n_neighbors=k_nn)
            # self.distances_sq = self.distances ** 2
            self.saved_X = X.copy()
            return self.distances, self.mask

    def transform_approx(self, X, k_nn=None, gamma=None, loocv=False):
        """ transform X.
        :param X: array, new samples
        :return: array, new transformed samples
        """
        #
        if gamma == None:
            gamma = self.gamma
        if self.verbose:
            print(f"[KLIMT] Using gamma = {gamma}")

        if loocv:
            if np.array_equal(X, None):
                X = self.samples_train
            assert np.array_equal(X, self.samples_train)

        if k_nn == None:
            k_nn = self.k_nn
        #
        N_new = X.shape[0]
        M = self.samples_train.shape[0]

        assert X.shape[1] == self.samples_train.shape[1]
        #
        distances, mask = self.search_neighbors(X, k_nn=k_nn)

        alpha_ = np.exp(- np.float128(gamma * distances**2.))
        #
        if loocv:
            alpha_[:,0] = 0

        self.alpha_unormed = alpha_.copy()
        #
        norm_ = alpha_.sum(axis=1, keepdims=True)
        norm_[norm_ == 0] = 1 # To exclude divide by 0
        alpha_ /= norm_
        # self.norm_sum = norm_

        mask_ravel = mask.ravel()
        # Deal with unbalanced classes:
        if self.class_weight =='balanced':
            self.sample_weights = compute_sample_weight('balanced', y=self.sample_classes_train)
            alpha_ones = self.sample_weights[mask_ravel].reshape(N_new, k_nn)
            alpha_ones /= alpha_ones.sum(axis=1, keepdims=True)
            alpha_ones *= k_nn
            alpha_ *= alpha_ones
            alpha_ /= alpha_.sum(axis=1, keepdims=True)
        else:
            self.sample_weights = np.ones([M]) #

        # Re-arrange with sparse matrix for fast computation
        row = np.arange(0, N_new, 1)
        row = np.repeat(row, k_nn, axis=0)
        col = mask_ravel
        sparse_mat = coo_array((alpha_.ravel(), (row, col)), shape=(N_new, M))
        #
        return X + sparse_mat @ self.translation

    def fit_transform(self, X, y=None, **fit_params):

        return self.fit(X, y, **fit_params).transform(X)
