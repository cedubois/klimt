# KLIMT

---
## Paper
Easily Interpretable, Non-parametric Sample Transformation for Classification. <br>
Joint work with Éric Debreuve and Jean-Olivier Irisson.<br>
`pdf/Dubois_Irisson_Debreuve_GRETSI2022.pdf` <br>
Poster : <br>
`pdf/gretsi-2022-poster.pdf`
## Usage
<i>sklearn-friendly</i><br>
Classification: <br>
`pred = KLIMT_CLF(gamma=1).fit(X_train, y_train).predict(X_test)`<br>
Transformation: <br>
`klimt = KLIMT(gamma=1)`<br>
`X_train_transformed = klimt.fit_transform(X_train, y_train)`<br>
`X_test_transformed = klimt.transform(X_test)`

## Examples
Evolution of the classification result as the parameter gamma increases.

<img src="./images/klimt-4classes.gif" width="600"/>

---

|               Gaussian-SVM                |                   k-nn                    |              Decision Tree               |                    KLIMT                    |                    KLIMT-SVM                     |
|:-----------------------------------------:|:-----------------------------------------:|:----------------------------------------:|:-------------------------------------------:|:------------------------------------------------:|
| <img src="./images/svm.png" width="150"/> | <img src="./images/knn.png" width="150"/> | <img src="./images/dt.png" width="150"/> | <img src="./images/klimt.png" width="150"/> | <img src="./images/klimt_svm_.png" width="150"/> |


## Author & License
Copyright UCA/CNRS/Inria
Contributor(s): Cedric Dubois 2022

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".