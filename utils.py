from sklearn.metrics import accuracy_score, balanced_accuracy_score, classification_report
import numpy as np


def print_pred_results(y_true, predictions):
    acc = np.round(100 * accuracy_score(y_true, predictions), 2)
    print(f"Accuracy : {acc}")

    acc_b = np.round(100 * balanced_accuracy_score(y_true, predictions), 2)
    print(f"Balanced Accuracy : {acc_b}")

    print(classification_report(y_true, predictions))

