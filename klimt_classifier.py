# Copyright UCA/CNRS/Inria
# Contributor(s): Cedric Dubois 2022
#
# cedric.dubois@inria.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#
#
import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin
from scipy.spatial.distance import cdist
from klimt import KLIMT

class KLIMT_CLF(BaseEstimator, ClassifierMixin):
    """
    KLIMT transformation + Nearest Target classifier (NT)
    BINARY classification ONLY
    """
    def __init__(self,
                gamma = 1,
                k_nn = 100,
                class_weight = None,
                raise_warning = True,
                verbose = 0,
                ):
       """
       Parameters
       ----------
       klimt transformer parameters:
       :param gamma: float, inverse kernel width
       :param k_nn: int, number of nearest neighbors to use for estimating the translations
       :param class_weight: str, None or 'balanced'
       :param raise_warning: bool
       :param verbose: int
       """
       self.gamma = gamma
       self.k_nn = k_nn
       self.raise_warning = raise_warning
       self.class_weight = class_weight
       self.verbose = verbose

       if verbose:
           print("[KLIMT CLF] Instantiate KLIMT Transformer")
       self.klimt_model = KLIMT(gamma,
                                 k_nn,
                                 class_weight,
                                 raise_warning,
                                 verbose)

    def fit(self, X, y):
        self.classes_ = np.unique(y)
        self.klimt_model.fit(X, y)
        self.targets = self.klimt_model.targets
        return self

    def compute_distances_to_targets(self, X):
        self.distance_to_targets = cdist(X, self.targets, metric='euclidean')

    def predict(self, X, gamma=None):
        if gamma==None:
            gamma = self.gamma
        X_tr = self.klimt_model.transform(X, k_nn=self.k_nn, gamma=gamma)
        self.compute_distances_to_targets(X_tr)
        return np.argmin(self.distance_to_targets, axis=1)

    def predict_proba(self, X, gamma=None):
        if gamma==None:
            gamma = self.gamma
        X_tr = self.klimt_model.transform(X, k_nn=self.k_nn, gamma=gamma)
        self.compute_distances_to_targets(X_tr)
        return np.exp( - self.distance_to_targets) / np.exp(- self.distance_to_targets).sum(axis=1, keepdims=True)
