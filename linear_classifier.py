# Copyright UCA/CNRS/Inria
# Contributor(s): Cedric Dubois 2022
#
# cedric.dubois@inria.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#
import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin
from scipy.spatial.distance import cdist

class nearest_target(BaseEstimator, ClassifierMixin):
    """
    Simplest classifier taking the class of the closer target
    :param:
    targets: targets coordinates order by label i.e 1st target is label 0.
    metric: Metric to use for the distance computation by scipy cdist function. See scipy doc.
    """
    def __init__(self,
                 targets,
                 metric='euclidean'):
        self.targets = targets
        self.metric = metric
        #
        self.flag_distances = True

    def fit(self,X, y=None):
        self.classes_ = np.unique(y)
        return self

    def compute_distances_to_targets(self, X):
        self.distance_to_targets = cdist(X, self.targets, metric=self.metric)
        self.flag_distances = False

    def predict(self, X):
        # if self.flag_distances:
        self.compute_distances_to_targets(X)
        classes = np.argmin(self.distance_to_targets, axis=1)
        return classes

    def predict_proba(self, X):
        # if self.flag_distances:
        self.compute_distances_to_targets(X)
        return np.exp( - self.distance_to_targets) / np.exp(- self.distance_to_targets).sum(axis=1, keepdims=True)
        # return 1 - np.exp(self.distance_to_targets) / np.exp(self.distance_to_targets).sum(axis=1, keepdims=True)
